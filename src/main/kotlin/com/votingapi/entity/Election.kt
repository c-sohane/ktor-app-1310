package com.votingapi.entity

import org.jetbrains.exposed.dao.IntIdTable
import org.jetbrains.exposed.sql.ReferenceOption

object Elections : IntIdTable() {
    val titre = varchar("titre", 255)
    val nombreDePlaces = integer("nombre_de_places")
}

data class Election(
    val id: Int,
    val titre: String,
    val nombreDePlaces: Int
)

package com.votingapi.entity

import org.jetbrains.exposed.dao.IntIdTable

object Bulletins : IntIdTable() {
    val classement = text("classement")
    val election = reference("election_id", Elections, onDelete = ReferenceOption.CASCADE)
}

data class Bulletin(
    val id: Int,
    val classement: List<String>,
    val electionId: Int
)

package com.votingapi.entity

import org.jetbrains.exposed.dao.IntIdTable

object Propositions : IntIdTable() {
    val name = varchar("name", 255)
    val election = reference("election_id", Elections, onDelete = ReferenceOption.CASCADE)
}

data class Proposition(
    val id: Int,
    val name: String,
    val electionId: Int
)

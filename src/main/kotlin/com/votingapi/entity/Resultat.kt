package com.votingapi.entity

import org.jetbrains.exposed.dao.IntIdTable

object Resultats : IntIdTable() {
    val election = reference("election_id", Elections, onDelete = ReferenceOption.CASCADE)
    val elected = text("elected")
    val ranking = text("ranking")
}

data class Resultat(
    val id: Int,
    val electionId: Int,
    val elected: List<String>,
    val ranking: List<String>
)

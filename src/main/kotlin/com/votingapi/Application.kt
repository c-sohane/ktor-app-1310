package com.votreapp

import io.ktor.server.application.*
import io.ktor.features.ContentNegotiation
import io.ktor.features.CORS
import io.ktor.gson.gson
import io.ktor.http.HttpMethod
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.exposed.dao.*

fun main() {
    Database.connect("jdbc:h2:mem:test;DB_CLOSE_DELAY=-1;", driver = "org.h2.Driver")
    transaction {
        SchemaUtils.create(Elections, Propositions, Bulletins)
    }

    embeddedServer(Netty, port = 8080) {
        install(ContentNegotiation) {
            gson {
                setPrettyPrinting()
            }
        }
        install(CORS) {
            method(HttpMethod.Options)
            method(HttpMethod.Get)
            method(HttpMethod.Post)
            method(HttpMethod.Put)
            method(HttpMethod.Delete)
            header("Authorization")
            header("Content-Type")
            allowCredentials = true
            allowNonSimpleContentTypes = true
            anyHost() // Permet toutes les origines. A configurer en production.
        }
        routing {
            route("/elections") {
                get {
                    val elections = transaction {
                        Election.all().map { it.toMap() }
                    }
                    call.respond(elections)
                }
                post {
                    val electionRequest = call.receive<ElectionRequest>()
                    val election = transaction {
                        Election.new {
                            titre = electionRequest.titre
                            nombreDePlaces = electionRequest.nombreDePlaces
                        }
                    }
                    call.respond(HttpStatusCode.Created, election.toMap())
                }
                get("/{id}") {
                    val id = call.parameters["id"]?.toIntOrNull()
                    if (id == null) {
                        call.respond(HttpStatusCode.BadRequest, "Invalid ID")
                        return@get
                    }
                    val election = transaction {
                        Election.findById(id)?.toMap()
                    }
                    if (election == null) {
                        call.respond(HttpStatusCode.NotFound, "Election not found")
                    } else {
                        call.respond(election)
                    }
                }
                delete("/{id}") {
                    val id = call.parameters["id"]?.toIntOrNull()
                    if (id == null) {
                        call.respond(HttpStatusCode.BadRequest, "Invalid ID")
                        return@delete
                    }
                    transaction {
                        Election.findById(id)?.delete()
                    }
                    call.respond(HttpStatusCode.NoContent)
                }
            }
        }
    }.start(wait = true)
}

data class ElectionRequest(val titre: String, val nombreDePlaces: Int)

fun Election.toMap() = mapOf(
    "id" to id.value,
    "titre" to titre,
    "nombreDePlaces" to nombreDePlaces
)

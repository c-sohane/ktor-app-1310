package com.votingapi

import com.votreapp.entity.*
import io.ktor.application.*
import io.ktor.features.ContentNegotiation
import io.ktor.features.StatusPages
import io.ktor.gson.gson
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.routing.*
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import javax.naming.OperationNotSupportedException

fun Application.module() {
    install(ContentNegotiation) {
        gson {
            setPrettyPrinting()
        }
    }

    install(StatusPages) {
        exception<OperationNotSupportedException> { cause ->
            call.respond(HttpStatusCode.BadRequest, cause.localizedMessage)
        }
    }

    Database.connect("jdbc:h2:mem:test;DB_CLOSE_DELAY=-1;", driver = "org.h2.Driver")
    transaction {
        SchemaUtils.create(Elections, Propositions, Resultats, Bulletins)
    }

    routing {
        route("/api/elections") {
            get {
                call.respond(transaction {
                    Election.findAll().map { it.toElection() }
                })
            }

            get("/{id}") {
                val id = call.parameters["id"]?.toIntOrNull() ?: throw OperationNotSupportedException("Invalid ID")
                call.respond(transaction {
                    Election.findById(id)?.toElection() ?: throw OperationNotSupportedException("Election not found")
                })
            }

            post {
                val election = call.receive<Election>()
                call.respond(transaction {
                    val newElection = Election.new {
                        titre = election.titre
                        nombreDePlaces = election.nombreDePlaces
                    }
                    newElection.toElection()
                })
            }

            delete("/{id}") {
                val id = call.parameters["id"]?.toIntOrNull() ?: throw OperationNotSupportedException("Invalid ID")
                transaction {
                    Election.findById(id)?.delete() ?: throw OperationNotSupportedException("Election not found")
                }
                call.respond(HttpStatusCode.NoContent)
            }
        }
    }
}

fun main() {
    embeddedServer(Netty, port = 8080, module = Application::module).start(wait = true)
}

fun ResultRow.toElection() = Election(
    id = this[Elections.id].value,
    titre = this[Elections.titre],
    nombreDePlaces = this[Elections.nombreDePlaces]
)
